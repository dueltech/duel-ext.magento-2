The duel-gallery extension has been designed to work in Magento 2 installations. The extension can be installed from Magento Marketplace, or by following the instructions below. If your store is running Magento 1, then please use https://github.com/duel-tech/duel-emails instead.

Magento marketplace link for this Magento 2 extension: https://marketplace.magento.com/duel-gallery.html

Once you have installed the extension, all information about how to use it can be found in this user guide: https://marketplace.magento.com/media/catalog/product/duel-gallery-1-0-6-ce/user_guides.pdf

Step 1: Using a command line tool, navigate to the root directory of your Magento 2 installation  (this folder is usually called “magento/” and should contain folders “app/” and “bin/”

Step 2. Run the command “composer require duel/gallery”, to install the extension from  packagist.org. When this has run, a folder called “duel” should now exist in your  <<magento_root>>/vendor/ directory.

Step 3. Run the following commands to enable the module - some may take a few minutes. 
    php -f bin/magento module:enable --clear-static-content Duel_Gallery     
    php -f bin/magento setup:upgrade     
    php -f bin/magento setup:static-content:deploy     
    php -f bin/magento cache:flush
    
Step 4: The last two commands should ensure that the module is installed *and* clear all  cached content. However, in some hosting environments there may be a permissions issue with  deleting cached static content from the command line. If upon loading your store homepage  there are still static-content related errors, then please delete these files through the admin  panel, using “System > Cache Management > Additional Cache Management >  Flush Static  Files”.

Step 5: After opening “Stores > Configuration”, the option “Duel” should now be available in the  sidebar. 

Alternatively steps 1 and 2 can be replaced by downloading the zip file from this repository (https://github.com/duel-tech/duel-gallery) using "Clone or download", extracting the folder, and placing the "Duel" folder into <<magento_root>>/app/code. Steps 3-5 can then be carried out as above.
